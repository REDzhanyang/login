<?php
	include("includes/coning.php");
	include("includes/classes/Accout.php");
	include("includes/classes/Constants.php");

	$accout = new Accout($conn);

	include("includes/handlers/register-handlers.php");
	include("includes/handlers/login-handlers.php");


	function getInputValue($name){
		if (isset($_POST[$name])) {
			echo $_POST[$name];
		}
	}

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>农业信息分享</title>
	<link rel="icon" type="image/x-icon" href="favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" type="text/css" href="../css/login.css">
</head>
<body background="../bj.png">
	<div id="top">
		<img src="../images/top.png" alt="" class="topimg">
		<span class="logo">
			<img class="logo" src="../favicon.ico" alt="">
		</span>
		<span class="daohang">
			<ul>
				<li>
					<a href="">最新信息</a>
				</li>
				<li>
					<a href="">信息分类</a>
				</li>
				<li>
					<a href="">发布信息</a>
				</li>
				<li>
					<a href="">节能环保</a>
				</li>
				<li>
					<a href="">技能手册</a>
				</li>
				<li>
					<a href="">关于我们</a>
				</li>
			</ul>
		</span>
	</div>
	<div id="inputContainer">
		<form action="register.php" id="loginForm" method="POST">
			<p><h2>登陆账户</h2></p>
			<p>
				<?php echo $accout->getError(Constants::$loginFailed); ?>
				<label for="loginUsername">账号：</label>
				<input 
					type="text"
					id="loginUsername" 
					name="loginUsername"
					placeholder="请输入您的用户名." 
					required 
					>
			</p>
			<p>
				<label for="loginPassword">密码：</label>
				<input 
					type="password"
					id="loginPassword" 
					name="loginPassword"
					placeholder="请输入您的密码." 
					required 
					>
			</p>
			<button type="submit" name="loginButton" >登陆</button>
		</form>

		<form action="register.php" id="loginForm" method="POST">
			<p><h2>注册账户</h2></p>
			<p>
				<?php echo $accout->getError(Constants::$usernameCharacters); ?>
				<?php echo $accout->getError(Constants::$usernameTaken); ?>
				<label for="username">账号：</label>
				<input 
					type="text"
					id="username" 
					name="username"
					placeholder="请输入您的用户名." 
					value="<?php getInputValue('username') ?>" 
					required 
					>
			</p>
			<p>
				<?php echo $accout->getError(Constants::$nameCharacters); ?>
				<label for="name">姓名：</label>
				<input 
					type="text"
					id="name" 
					name="name"
					placeholder="请输入您的真实姓名." 
					value="<?php getInputValue('name') ?>" 
					required 
					>
			</p>
			<p>
				<?php echo $accout->getError(Constants::$emailCharacters); ?>
				<?php echo $accout->getError(Constants::$emailTaken); ?>
				<label for="email">邮箱：</label>
				<input 
					type="text"
					id="email" 
					name="email"
					placeholder="请输入您邮箱地址." 
					value="<?php getInputValue('email') ?>" 
					required 
					>
			</p>
			<p>
				<?php echo $accout->getError(Constants::$passwordNotMatch); ?>
				<?php echo $accout->getError(Constants::$passwordWithNumAndLetter); ?>
				<?php echo $accout->getError(Constants::$passwordCharacters); ?>
				<label for="password">密码：</label>
				<input 
					type="password"
					id="password" 
					name="password"
					placeholder="请输入您的密码." 
					value="" 
					required 
					>
			</p>
			<p>
				<label for="password2">确认：</label>
				<input 
					type="password"
					id="password2" 
					name="password2"
					placeholder="请确认您的密码." 
					value="" 
					required 
					>
			</p>
			<button type="submit" name="registerButton" >注册</button>
		</form>
	</div>
</body>
</html>