<?php 
	class Accout{
		private $errorArray;
		private $con;
		public function __construct($conn){
			$this->con = $conn;
			$this->errorArray = array();
		}

		

		public function login($username,$password){
			$password = md5($password);
			$sql = "SELETC * FROM users WHERE username='$username' AND password='$password' ";
			$query = mysqli_query($this->con,$sql);

			if (mysqli_num_rows($query) == 1) {
				return true;
			}else{
				array_push($this->errorArray,Constants::$loginFailed);
				return false;
			}
		}


		public function register($username,$name,$email,$password,$password2){
			$this->validateUsername($username);
			$this->validateName($name);
			$this->validateEmail($email);
			$this->validatePassword($password,$password2);

			if (empty($this->errorArray) == true) {
				return $this->insertUserDetails($username,$name,$email,$password);
			}else{
				return false;
			}
		}

		public function insertUserDetails($username,$name,$email,$password){
			$password = md5($password);
			$profilePic = "images/touxiang.png";
			$date = date("Y-m-d");
			$sql = "INSERT INTO users VALUES('','$username','$name','email','$password','$date','$profilePic')";
			$result = mysqli_query($this->con,$sql);
			return $result;
		}


		public function getError($error){
			if (!in_array($error, $this->errorArray)) {
				$error = "";
			}
			return "<span class='errorMessage'>$error</span>";
		}

		private function validateUsername($un){
			if (strlen($un) > 25 || strlen($un) < 5) {
				array_push($this->errorArray,Constants::$usernameCharacters);
				return;
			}

			$sql = "SELECT username FROM users WHERE username='$un'";
			$checkUsername = mysqli_query($this->con,$sql);
			if (mysqli_num_rows($checkUsername) != 0) {
				array_push($this->errorArray, Constants::$usernameTaken);
				return;
			}
		}

		private function validateName($nm){
			if (strlen($nm) > 25 || strlen($nm) < 2) {
				array_push($this->errorArray,Constants::$nameCharacters);
				return;
			}
		}

		private function validateEmail($em){
			if (!filter_var($em,FILTER_VALIDATE_EMAIL)) {
				array_push($this->errorArray,Constants::$emailCharacters);
				return;
			}
			$sql = "SELECT email FROM users WHERE email='$em'";
			$checkEmail = mysqli_query($this->con,$sql);
			if (mysqli_num_rows($checkEmail) != 0) {
				array_push($this->errorArray, Constants::$emailTaken);
				return;
			}
		}

		private function validatePassword($pw,$pw2){
			if ($pw !=$pw2) {
				array_push($this->errorArray,Constants::$passwordNotMatch);
				return;
			}

			if (preg_match('/[^A-Za-z0-9]/', $pw)) {
				array_push($this->errorArray,Constants::$passwordWithNumAndLetter);
				return;
			}

			if (strlen($pw) > 25 || strlen($pw) < 5) {
				array_push($this->errorArray,Constants::$passwordCharacters);
				return;
			}
		}
	}



 ?>