<?php 

	/**
	 * 
	 */
	//注册提示
	class Constants{
		public static $usernameCharacters = "用户名必须在5~25位之间！";
		public static $nameCharacters = "姓名必须在2~25位之间！";
		public static $emailCharacters = "邮箱地址不合法！";
		public static $passwordNotMatch = "两次密码不一致！";
		public static $passwordWithNumAndLetter = "密码必须是字母或数字！";
		public static $passwordCharacters = "密码必须在5~25位之间！";
		public static $emailTaken = "邮箱已被注册！";
		public static $usernameTaken = "用户名已被注册！";


		//登陆提示
		public static $loginFailed = "用户名或密码错误！";
	}



 ?>